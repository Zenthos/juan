/** *********************************************************************** **
 ** *********************************************************************** **
 **                                                                         **
 ** @file           main.c                                                  **
 ** @brief          File input/output functions for bash++ major assignment **
 **                                                                         **
 ** @author         Ryan Porterfield                                        **
 ** @author         Juan Hernandez                                          **
 ** @author         Noel Behailu                                            **
 ** @author         Erika Gutierrez                                         **
 ** @copyright      BSD 3 Clause (See COPYING file)                         **
 **                                                                         **
 ** *********************************************************************** **
 ** *********************************************************************** **/


#include <bash++.h>

#include <iso646.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <io.h>
#include <parser.h>


const char  *help[]         =   { "-h", "--help" };
const char  *verbose[]      =   { "-v", "--verbose" };
const char  *version[]      =   { "-V", "--version" };


int
main (int argc, const char **argv)
{
    ProgramData data = NULL;
    ExecStatus stat = exec_is_valid (argc, argv, &data);
    if (INVALID == stat)
        return EXIT_FAILURE;
    else if (PRINT_INFO == stat)
        return EXIT_SUCCESS;
    get_o_filename (data);
    if (data->verbose) {
        printf ("Reading from bash++ input file %s\n", data->i_name);
        printf ("Writing to bash output file %s\n", data->o_name);
    }
    run (data);
    free (data->o_name);
    free (data);
    return EXIT_SUCCESS;
}


/** @brief          Checks if the program was started with correct arguments.
 *
 *  @details        The program is started correctly if a bash++ file name is
 *                  passed, or if the help or version flags are passed.
 *  @param argc     Number of command line arguments passed to the program.
 *  @param argv     Character pointer (string) array of command line arguments.
 *  @param data     A structure continaing all data for this instance of the
 *                  programs execution.
 *  @return         @link VALID @endlink if program execution should procede,
 *                  @link INVALID @endlink if the program should exit with an
 *                  error, or @link PRINT_INFO @endlink if the program should
 *                  exit successfully after printing either help or version.
 */
ExecStatus
exec_is_valid (int argc, const char **argv, ProgramData *data)
{
    const char *exec_name = &argv[0][2];
    *data = parse_args (argc, argv, exec_name);
    if ((argc < 2) or ((*data)->verbose and argc < 3)) {
        print_help (*data);
        return INVALID;
    } else if (print_info (*data))
        return PRINT_INFO;
    else if (NULL == (*data)->i_name)
        return INVALID;
    return VALID;
}


/** @brief          Initializes a ProgramData struct for the program.
 *
 *  @details        Uses the flags passed to create a ProgramData structure
 *                  containing all relevant program data for the running
 *                  instance of the program.
 *  @param exec     The name of the executable
 *  @param i_name   The name of the input bash++ file
 *  @param flags    An integer whose bits are set based on command line
 *                  arguments passed at run time.
 *  @return         A new dynamically allocated instance of ProgramData
 */
ProgramData
init_data (const char *exec, const char *i_name, int flags)
{
    ProgramData data = malloc (sizeof (struct ProgramData_s));
    /* Trick to initialize constant struct members */
    struct ProgramData_s init = {
        .exec       = exec,
        .i_name     = i_name,
        .help       = (flags & HELP_BIT) ? 1 : 0,
        .version    = (flags & VERSION_BIT) ? 1 : 0,
        .verbose    = (flags & VERBOSE_BIT) ? 1 : 0
    };
    memcpy (data, &init, sizeof (*data));
    return data;
}


/** @brief          Parses command line arguments and sets internal flags
 *                  accordingly
 *
 *  @param argc     Number of command line arguments passed to the program.
 *  @param argv     Character pointer (string) array of command line arguments.
 *  @return         An integer variable with bits set according to flags
 *                  passed. Effectively a boolean array.
 */
ProgramData
parse_args (int argc, const char **argv, const char *exec)
{
    const char *i_name = NULL;
    int flags = 0;
    for (int i = 1; i < argc; ++i) {
        const char *arg = argv[i];
        if (not strcmp (arg, help[0]) or not strcmp (arg, help[1]))
            flags |= HELP_BIT;
        else if (not strcmp (arg, verbose[0]) or not strcmp (arg, verbose[1]))
            flags |= VERBOSE_BIT;
        else if (not strcmp (arg, version[0]) or not strcmp (arg, version[1]))
            flags |= VERSION_BIT;
        else
            i_name = argv[i];
    }
    return init_data (exec, i_name, flags);
}


/** @brief          Print program usage and flag information.
 *
 *  @details        Print's program usage information when program called with
 *                  -h or --help. Presumably the program exits after this but
 *                  the function behavior does not require it.
 *  @param data     A structure continaing all data for this instance of the
 *                  programs execution.
 */
void
print_help (ProgramData data)
{
    printf ("Usage: %s [OPTION] <file>\n", data->exec);
    printf ("Parses a bash++ file and outputs a bash file.\n\n");
    printf ("  <file>\t\tthe bash++ file being converted to a bash file\n");
    printf ("  %s, %s\t\tshow this help message and exit\n", help[0],
            help[1]);
    printf ("  %s, %s\t\tshow debugging messages during execution\n",
            verbose[0], verbose[1]);
    printf ("  %s, %s\t\tshow program's version number and exit\n",
            version[0], version[1]);
}


/** @brief          Checks if help or version needs to be printed.
 *
 *  @details        If the program was called with (-h, --help) or 
 *                  (-v, --version) the appropriate print function is called
 *                  and the function returns true (1).
 *  @param data     A structure continaing all data for this instance of the
 *                  programs execution.
 *  @return         true (1) if -h, --help, -v, or --version was called or
 *                  false (0) if not.
 */
int
print_info (ProgramData data)
{
    if (data->help) {
        print_help (data);
        return 1;
    } else if (data->version) {
        print_version (data);
        return 1;
    }
    return 0;
}


/** @brief          Print program version number, copyright, and disclaimer.
 *
 *  @details        Print's program's version number, copyright line, and
 *                  disclaimer when program is called with -v or --version
 *                  Presumably the program exits after this but the function
 *                  behavior does not require it.
 *  @param data     A structure continaing all data for this instance of the
 *                  programs execution.
 */
void
print_version (ProgramData data)
{
    printf ("%s %s %s (%s)\n", data->exec, BASHPP_VERSION, BASHPP_BUILD_DATE,
            BASHPP_BUILD_STATUS);
    printf ("%s\n%s\n", BASHPP_COPYRIGHT, BASHPP_DISCLAIMER);
}


/** *********************************************************************** **
 **                                   EOF                                   **
 ** *********************************************************************** **/
