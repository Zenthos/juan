/** *********************************************************************** **
 ** *********************************************************************** **
 **                                                                         **
 ** @file           io.h                                                    **
 ** @brief          File input/output functions for bash++ major assignment **
 **                                                                         **
 ** @author         Ryan Porterfield                                        **
 ** @author         Juan Hernandez                                          **
 ** @author         Noel Behailu                                            **
 ** @author         Erika Gutierrez                                         **
 ** @copyright      BSD 3 Clause (See COPYING file)                         **
 **                                                                         **
 ** *********************************************************************** **
 ** *********************************************************************** **/


#ifndef BASHPP_IO_H
#define BASHPP_IO_H


#include <bash++.h>


int         get_o_filename      (ProgramData data);
int         run                 (ProgramData data);


#endif /* BASHPP_IO_H */


/** *********************************************************************** **
 **                                   EOF                                   **
 ** *********************************************************************** **/
