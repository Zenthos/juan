/** *********************************************************************** **
 ** *********************************************************************** **
 **                                                                         **
 ** @file           bash++.h                                                **
 ** @brief          General functions for bash++ parser                     **
 **                                                                         **
 ** @author         Ryan Porterfield                                        **
 ** @author         Juan Hernandez                                          **
 ** @author         Noel Behailu                                            **
 ** @author         Erika Gutierrez                                         **
 ** @copyright      BSD 3 Clause (See COPYING file)                         **
 ** @version        1.0.0:2014-10-02                                        **
 **                                                                         **
 ** *********************************************************************** **
 ** *********************************************************************** **/


#ifndef BASHPP_H
#define BASHPP_H


#define BASHPP_BUILD_DATE   "2014-10-02"
#define BASHPP_BUILD_STATUS "Release"
#define BASHPP_COPYRIGHT    "Copyright (C) 2014 CSCE 3600-001 Group 21"
#define BASHPP_DISCLAIMER   "This is free software; see the source for " \
                            "copying conditions.  There is NO\nwarranty; not " \
                            "even for MERCHANTABILITY or FITNESS FOR A " \
                            "PARTICULAR PURPOSE"
#define BASHPP_VERSION      "1.0.0"

#define HELP_BIT            (1 << 0)
#define VERBOSE_BIT         (1 << 1)
#define VERSION_BIT         (1 << 2)


/** @struct         ProgramData
 *  @brief          Stores basic program data used by most functions.
 *
 */
typedef struct ProgramData_s {
    char        *o_name;    /**< The name of the bash output file */
    const char  *exec;      /**< The name of the executable */
    const char  *i_name;    /**< The name of the bash++ input file */
    const int   help;       /**< Does the program usage need to be printed */
    const int   version;    /**< Does the program version need to be printed */
    const int   verbose;    /**< Is the program is in verbose mode */
} *ProgramData;


/** @enum           ExecStatus
 *  @brief          Possible program execution states
 *
 *  @details        Either the program was executed properly with a bas++
 *                  filename passed, or the program was executed with the help
 *                  flag or version flag passed, or the program was executed
 *                  incorrectly.
 */
typedef enum {
    VALID,      /**< The execution of the program is valid */
    INVALID,    /**< The execution of the program is invalid */
    PRINT_INFO  /**< Print some form of program info and exit successfully */
} ExecStatus;

ExecStatus  exec_is_valid       (int argc, const char **argv, ProgramData *data);
ProgramData init_data           (const char *exec, const char *i_name,
        int flags);
ProgramData parse_args          (int argc, const char **argv, const char *exec);
void        print_help          (ProgramData data);
int         print_info          (ProgramData data);
void        print_version       (ProgramData data);


#endif /* BASHPP_H */


/** *********************************************************************** **
 **                                   EOF                                   **
 ** *********************************************************************** **/
