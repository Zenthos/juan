Bourne Again Shell ++
=====================

Authors:

* Juan Hernandez <JuanHernandez12@my.unt.edu>
* Erika Gutierrez <ErikaCristinaGutierrezPortillo@my.unt.edu>
* Noel Behailu <NoelBehailu@my.unt.edu>
* Ryan Porterfield <RyanPorterfield@my.unt.edu>

Date:       2014-09-25  
Version:    0.0.1  


About
-----
Bourne Again SHell ++ (bash++) is a homework assignment for CSCE 3600.  
It aims to be a simple wrapper around bash that adds a few usablilty  
features and syntax modifications. The major differences are:  

1.  Whitespace around equals '=' signs. bash++ allows value assignment  
    of variables with the syntax `var = val` as well as the traditional  
    `var=val`
2.  `then` on the same line as `if` without the need for a semicolon `;`
3.  A simpler syntax for sentinel controlled for loops. Instead of  
    needing to say

        for i in {0..$var}; do
            ...
        done

    you can use the syntax

        repeat $var times
        {
            ...
        }


LICENSE
-------
Copyright (c) 2014, CSCE 3600 Group 21
All rights reserved.  
  
Redistribution and use in source and binary forms, with or without  
modification, are permitted provided that the following conditions  
are met:  
  
1. Redistributions of source code must retain the above copyright  
notice, this list of conditions and the following disclaimer.  
2. Redistributions in binary form must reproduce the above copyright  
notice, this list of conditions and the following disclaimer in the  
documentation and/or other materials provided with the distribution.  
3. Neither the name of the copyright holder nor the names of its  
contributors may be used to endorse or promote products derived from  
this software without specific prior written permission.  
   
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS  
IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED  
TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A  
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  
TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR  
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF  
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING  
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS  
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.  
